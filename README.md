# Projet 1 JAVA JEE 5A - QURIE Omar & LABAT Basile

**[FR]**

## 1) Installation du projet 

T�l�chargez le projet via le lien github correspondant.

## 2) Initialisation du projet 

Nous supposons que vous avez d�ja install� mysql connector driver.
 
Il vous faudra alors : 	
* cr�ez une database sur mySQL avec un user et mot de passe et donnez lui toutes les permissions (create, update, delete, ...)
* cr�ez une datasource du m�me nom que celle de mySQL via le panneau d'administration wildly 
* passez lui le m�me user et mot de passe que sur mySQL + JNDI: java:/MySqlDS en param�tres de la datasource
* choisissez le driver mySQL connector appropri� et v�rifiez que le URL de connexion comporte le m�me nom que la database mySQL.


## 3) Testez le !

## 4) Fonctionalit�s pr�sentes 

Toute la base du projet est termin�e (liens, cr�ation dans la bdd, compteur, ...)

Fonctionalit�s annexes :  
* v�rifications de doublons pour les formulaires de promotions et membres et �galement lors de la modification d'un membre.
* selecteur de dates et heures via boostrap 3 bas� sur le pays ici "France" et la date actuelle
* pagination dynamique de la liste des membres	
* validation des formulaires via JQuery.  


**[EN]**

## 1) Project's installation 

Download the project thanks to the following github's link.

## 2) Project's initialization  

We assume that you have aldready download and install mysql connector driver.
 
We will need to:  
* create a database on mySQL with a user and password and give himm all permissions (create, update, delete, ...)
* create a datasource with the same name as on mySQL throught wildfly admin panel 
* for settings give him the same user and password as on mySQL + JNDI: java:/MySqlDS 
* choose mySQL connector driver that you have aldready downloaded and check that connection's URL include the same name than mySQL's database.
	

## 3) Have fun testing it !

## 4) Existing Fonctionalities 

Core project is completed
Side fonctionalities :  
* duplicate verification when adding a new Promo, member or when a member is modified 
* bootstrap 3 datetime picker base on local and actual date			
* Member list dynamic pagination 
* JQuery front validation.


*Copyright - EPF 2017*

