package model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Promotion {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long promoId;
	private String promoName;
	private int promoSize;

	@OneToMany(mappedBy = "promo")
	private List<Member> memberList;

	@OneToMany(mappedBy = "promo")
	private List<Event> eventList;

	public Promotion() {

	}

	public Promotion(String promoName, int promoSize) {
		this.promoName = promoName;
		this.promoSize = promoSize;
		this.eventList = new ArrayList<>();
		this.memberList = new ArrayList<>();
	}

	public Long getPromoId() {
		return promoId;
	}

	public void setPromoId(Long promoId) {
		this.promoId = promoId;
	}

	public int getPromoSize() {
		return promoSize;
	}

	public void setPromoSize(int promoSize) {
		this.promoSize = promoSize;
	}

	public void incrementSize() {
		this.promoSize++;
	}

	public void decrementSize() {
		this.promoSize--;
	}

	public String getPromoName() {
		return promoName;
	}

	public void setPromoName(String promoName) {
		this.promoName = promoName;
	}

	public List<Member> getMemberList() {
		return memberList;
	}

	public int getMembreListSize() {
		return memberList.size();
	}

	public void setMemberList(List<Member> memberList) {
		this.memberList = memberList;
	}

	public void addMember(Member m) {
		this.memberList.add(m);
	}

	public List<Event> getEventList() {
		return eventList;
	}

	public void setEventList(List<Event> eventList) {
		this.eventList = eventList;
	}

	public void addEvent(Event e) {
		this.eventList.add(e);
	}
}
