package dao;

import java.util.List;

import javax.ejb.SessionContext;
import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import model.Member;
import model.Promotion;

@Singleton
public class MemberDao {

	@PersistenceContext
	private EntityManager em;

	// Creates a new db entry
	public void save(Member m) {
		em.persist(m);
	}

	// Update an existing entry
	public void update(Member m) {
		em.merge(m);
	}

	// Deletes a member
	public void delete(Member m) {
		// Check if the instance is a managed entity instance belonging to the current
		// persistence context
		if (em.contains(m))
			em.remove(m);
		else
			em.remove(em.merge(m));
		// If not merge the state of the given entity into the current persistence
		// context and delete it
	}

	public Member findOne(Long id) {
		return em.find(Member.class, id);
	}

	public List<Member> findAll() {
		return em.createQuery("FROM Member").getResultList();
	}

	// Finds a member with an email returns null if none is found
	public Member findEmailMatch(String s) {
		List<Member> resultList = em.createQuery("FROM Member WHERE email='" + s + "'").getResultList();
		if (resultList.size() > 0)
			return resultList.get(0);
		else
			return null;
	}

}
