package dao;

import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Event;

@Singleton
public class EventDao {

	@PersistenceContext
	private EntityManager em;

	public void save(Event e) {
		em.persist(e);
	}

	public Event findOne(Long id) {
		return em.find(Event.class, id);
	}

	public List<Event> findAll() {
		return em.createQuery("FROM Event").getResultList();
	}

}
