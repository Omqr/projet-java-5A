package dao;

import java.util.List;

import javax.ejb.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Promotion;

@Singleton
public class PromotionDao {

	@PersistenceContext
	private EntityManager em;

	public void save(Promotion p) {
		em.persist(p);
	}

	// Update an existing entry
	public void update(Promotion p) {
		em.merge(p);
	}

	// Finds a promotion with an id
	public Promotion findOne(Long id) {
		return em.find(Promotion.class, id);
	}

	// Finds a promotion with a name, returns null if none is found
	public Promotion findMatch(String s) {
		List<Promotion> resultList = em.createQuery("FROM Promotion WHERE promoName='" + s + "'").getResultList();
		if (resultList.size() > 0)
			return resultList.get(0);
		else
			return null;
	}

	public List<Promotion> findAll() {
		return em.createQuery("FROM Promotion").getResultList();
	}

}
