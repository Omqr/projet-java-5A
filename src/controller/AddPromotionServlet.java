package controller;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.PromotionDao;
import model.Promotion;

@WebServlet("/add-promotion")
public class AddPromotionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// Controller for the page allowing to add a promotion

	@Inject
	private PromotionDao promoDao;

	public AddPromotionServlet() {
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher("WEB-INF/add_promotion.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// Parse user's inputs and check for name duplicates
		Promotion p = parsePromo(req);
		if (p != null) {
			// No duplicates, it is safe to create new promotion object and new db entry
			req.getSession().setAttribute("promo", p);
			promoDao.save(p);
			resp.sendRedirect("index?status=ok");
		} else {
			// refresh current page and display an error message
			resp.sendRedirect("add-promotion?status=error");
		}
	}

	private Promotion parsePromo(HttpServletRequest req) {
		// returns a new Promotion if one with the same name doesn't already exist
		String name = req.getParameter("name");
		Promotion promo = promoDao.findMatch(name);
		if (promo == null)
			return new Promotion(name, 0);
		else
			return null;
	}
}
