package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.MemberDao;
import dao.PromotionDao;
import model.Member;
import model.Promotion;

@WebServlet("/add-member")
public class AddMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// Controller for the page allowing to add a member

	@Inject
	MemberDao memberDao;
	@Inject
	PromotionDao promoDao;

	private String currentDate;
	private Promotion promo;

	public AddMemberServlet() {
		setCurrentDate();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("promos", promoDao.findAll());
		request.setAttribute("now", currentDate);
		request.getRequestDispatcher("WEB-INF/add_member.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// Parse users's inputs and check for email duplicates
		Member m = parseMember(req);
		if (m != null) {
			// If no email duplicate create a new member with the user's inputs
			// and save it in db via memberDao
			req.getSession().setAttribute("member", m);
			memberDao.save(m);
			incrementPromoSize(m);
			resp.sendRedirect("index");
		} else {
			// If the email is already user, the page is reloaded with a message error
			resp.sendRedirect("add-member?status=error");
		}
	}

	private Member parseMember(HttpServletRequest req) {
		String email = req.getParameter("email");
		String bDay = req.getParameter("bday");
		String name = req.getParameter("name");
		// Find the choosen promotion with its name
		promo = promoDao.findMatch(req.getParameter("promotion"));
		if (checkDuplicate(email)) {
			// If the email is not already used, create a new member object
			return new Member(name, email, bDay, promo);
		} else {
			// If the email is already used, return null to wave a flag and send user's
			// inputs via attributes
			req.getSession().setAttribute("email", email);
			req.getSession().setAttribute("bday", bDay);
			req.getSession().setAttribute("name", name);
			req.getSession().setAttribute("promo", promo);
			return null;
		}

	}

	private boolean checkDuplicate(String email) {
		// returns true if no Duplicate
		Member member = memberDao.findEmailMatch(email);
		return member == null;
	}

	private void setCurrentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		currentDate = sdf.format(new Date());
	}

	private void incrementPromoSize(Member m) {
		// Increments promo size and save new value in db
		Promotion p = m.getPromo();
		p.incrementSize();
		promoDao.update(p);
	}
}
