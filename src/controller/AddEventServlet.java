package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.EventDao;
import dao.PromotionDao;
import model.Event;
import model.Promotion;

@WebServlet("/add-event")
public class AddEventServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// Controller for the page allowing to add an event

	private String currentDate;

	@Inject
	private EventDao eventDao;
	@Inject
	private PromotionDao promoDao;

	public AddEventServlet() {
		updateCurrentDate();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Get all promotions and current date to send them to the view
		request.setAttribute("promos", promoDao.findAll());
		request.setAttribute("now", currentDate);
		request.getRequestDispatcher("WEB-INF/add_event.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		Event e = parseEvent(req);
		req.getSession().setAttribute("event", e);
		eventDao.save(e);
		resp.sendRedirect("index");
	}

	private Event parseEvent(HttpServletRequest req) {
		String name = req.getParameter("name");
		String description = req.getParameter("description");
		String dateTime = req.getParameter("dateTime");
		Promotion promo = promoDao.findMatch(req.getParameter("promotion"));
		return new Event(name, description, dateTime, promo);
	}

	public void updateCurrentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm");
		currentDate = sdf.format(new Date());
	}

}
