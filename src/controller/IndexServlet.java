package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EventDao;
import dao.MemberDao;
import dao.PromotionDao;
import model.Member;

@WebServlet("/index")
public class IndexServlet extends HttpServlet {

	// Controller for the index (dashboard) page

	private static final long serialVersionUID = 1L;

	@Inject
	MemberDao memberDao;
	@Inject
	PromotionDao promoDao;
	@Inject
	EventDao eventDao;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Get all the data to display on index page
		List<Member> memberList = memberDao.findAll();
		request.setAttribute("promos", promoDao.findAll());
		request.setAttribute("members", memberList);
		request.setAttribute("events", eventDao.findAll());

		// Defines the members table maximum items to display by page
		int maxEntriesPerPage = 5;
		int page = 1;

		// If there is a pageNumber parameter in the url, read it and store it in a
		// variable
		String pageNumberValue = request.getParameter("pageNumber");

		if (pageNumberValue != null) {
			try {
				page = Integer.parseInt(pageNumberValue);
				System.out.println("Page Number:" + page);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
		}

		// The offset variable defines the row of the first item to display
		// for example: page 1 start at item 0, page 2 item 5 ...
		int offset = maxEntriesPerPage * (page - 1);

		HttpSession httpSession = request.getSession();

		// Send list of Integers containing the number of each page
		// For example if there are 6 pages the list will contain numbers from 1 to 6
		httpSession.setAttribute("pages", getPages(memberList, maxEntriesPerPage));

		// Send the list of members to display to the view. List is fetched but method
		// "getListByOffsetAndLength"
		httpSession.setAttribute("displayedMembers", getListByOffsetAndLength(offset, maxEntriesPerPage, memberList));

		// Once all attributes are set, display the view
		request.getRequestDispatcher("WEB-INF/index.jsp").forward(request, response);
	}

	// Get list of Integers containing the number of each page
	// according to the entire memberList and the number of entries by page
	public ArrayList<Integer> getPages(List<Member> memberList, int maxEntriesPerPage) {
		ArrayList<Integer> pageNumbers = new ArrayList<>();
		int pages = memberList.size() / maxEntriesPerPage;
		if (memberList.size() % maxEntriesPerPage != 0) {
			// If list size=6 and max entries=5 will return 1
			// the value of pages is 6/5 rounded to the closest integer, so pages=1
			// But we will need 2 pages to display the 6 elements, so add 1
			pages = pages + 1;
		}
		for (int i = 1; i <= pages; i++) {
			// Populate the pageNumbers list
			pageNumbers.add(new Integer(i));
		}
		return pageNumbers;
	}

	// Gets the list of members to display
	public ArrayList<Member> getListByOffsetAndLength(int offset, int maxEntriesPerPage, List<Member> orginalList) {
		ArrayList<Member> displayedList = new ArrayList<>();
		// The offset variable defines the index of the first item to display
		// While the index of the last element to display is calculated by adding the
		// number of entries by page to the offset
		int lastElementIndex = offset + maxEntriesPerPage;
		// If the calculated value is bigger than the original list's size, set its
		// value at the list's size
		if (lastElementIndex > orginalList.size())
			lastElementIndex = orginalList.size();
		for (int i = offset; i < lastElementIndex; i++) {
			displayedList.add(orginalList.get(i));
			// Populate the list of items to display with the right items from the original
			// list
		}
		return displayedList;
	}
}
