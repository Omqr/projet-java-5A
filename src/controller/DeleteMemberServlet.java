package controller;

import java.io.IOException;
import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.MemberDao;
import dao.PromotionDao;
import model.Member;
import model.Promotion;

@WebServlet("/delete-member")
public class DeleteMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// Controller for the page allowing to delete a member

	@Inject
	MemberDao memberDao;
	@Inject
	PromotionDao promoDao;

	private Member oldMember;

	public DeleteMemberServlet() {
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Gets id through request url parameters to display the right member data
		Long id = Long.parseLong(request.getParameter("id"));
		oldMember = memberDao.findOne(id);
		oldMember.setMemberId(id);
		request.setAttribute("member", oldMember);
		request.getRequestDispatcher("WEB-INF/delete_membre.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 2 possibilities either the user chooses to delete the member and request come
		// with parameter "ok"
		// or he changes his mind and request come with parameter "cancel"
		// no matter the choice, redirect to index
		if (req.getParameter("ok") != null) {
			// delete member and decrease promotion size
			memberDao.delete(oldMember);
			decrementPromoSize(oldMember);
		}
		resp.sendRedirect("index");
	}

	public void decrementPromoSize(Member m) {
		// Gets the member's promotion, decreases size and saves new value in db
		Promotion p = m.getPromo();
		p.decrementSize();
		promoDao.update(p);
	}

}
