package controller;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.MemberDao;
import dao.PromotionDao;
import model.Member;
import model.Promotion;

@WebServlet("/update-member")
public class UpdateMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	// Controller for the page allowing to update a member

	@Inject
	MemberDao memberDao;
	@Inject
	PromotionDao promoDao;

	private Member oldMember;
	private String currentDate;

	public UpdateMemberServlet() {
		setCurrentDate();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setAttribute("promos", promoDao.findAll());
		request.setAttribute("now", currentDate);

		Long id = Long.parseLong(request.getParameter("id"));
		Member m = memberDao.findOne(id);
		// Set member id to update old member instead of creating a new entry
		m.setMemberId(id);
		oldMember = m;
		request.setAttribute("member", m);

		request.getRequestDispatcher("WEB-INF/update_membre.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// Parses user's inputs and checks for duplicate
		Member newMember = parseMember(req);
		if (newMember != null) {
			// No duplicate, it is safe to update the member's fields
			// Bind newMember to oldMember by giving them the same id
			newMember.setMemberId(oldMember.getMemberId());
			req.getSession().setAttribute("member", newMember);
			memberDao.update(newMember);
			// Update promotion size
			updatePromoMemberCount(newMember, oldMember);
			resp.sendRedirect("index");
		} else {
			// There is an error with the email address. The new email address is already
			// used by another member
			// Refresh page with an error message
			resp.sendRedirect("add-member?status=error");
		}
	}

	// Parses user's inputs and checks for duplicate
	private Member parseMember(HttpServletRequest req) {
		String name = req.getParameter("name");
		String email = req.getParameter("email");
		String bDay = req.getParameter("bday");
		Promotion promo = promoDao.findMatch(req.getParameter("promotion"));
		// If the user didn't change the email, there is no need to check for duplicate
		if (oldMember.getEmail().equals(email)) {
			return new Member(name, email, bDay, promo);
		} else {
			// The user changed the email, we need to check it this new email isn't already
			// used
			if (checkDuplicate(email)) {
				// The email address is not already used
				return new Member(name, email, bDay, promo);
			} else {
				// The email address is used by someone else, send the inputs individually to be
				// displayed
				// in the view instead of displaying empty form
				req.getSession().setAttribute("email", email);
				req.getSession().setAttribute("bday", bDay);
				req.getSession().setAttribute("name", name);
				req.getSession().setAttribute("promo", promo);
				return null;
			}
		}
	}

	private boolean checkDuplicate(String email) {
		// returns true if no Duplicate
		return memberDao.findEmailMatch(email) == null;
	}

	// Updates promotion size if the member changes his promotion
	private void updatePromoMemberCount(Member newMember, Member oldMember) {
		Promotion newPromo = newMember.getPromo();
		Promotion oldPromo = oldMember.getPromo();
		if (oldPromo != newPromo) {
			// If a change was done, change sizes accordingly and update db value
			oldPromo.decrementSize();
			newPromo.incrementSize();
			promoDao.update(oldPromo);
			promoDao.update(newPromo);
		}
	}

	private void setCurrentDate() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		currentDate = sdf.format(new Date());
	}
}
