<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	
	<title>When Is My Code Review?</title>
	
	<!-- Bootstrap CSS -->
	<link href="bower_components/bootstrap/dist/css/bootstrap.min.css"
		rel="stylesheet">
	<link
		href="bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css"
		rel="stylesheet">
	<link
		href="bower_components/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker-standalone.css"
		rel="stylesheet">
	
	<!-- Custom CSS -->
	<link href="css/style.css" rel="stylesheet">
	
	<!-- Custom Fonts -->
	<link
		href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
		rel="stylesheet" type="text/css">

</head>

<body>

	<div id="wrapper">

		<!-- Navigation -->
		<nav class="navbar navbar-default navbar-static-top container-fluid"
			role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/jee-training/index">When Is My
					Code Review?</a>
			</div>
			<!-- /.navbar-header -->

			<ul class="nav navbar-top-links navbar-right">
				<li class="dropdown"><a class="dropdown-toggle navlink"
					data-toggle="dropdown" href="#"> <i class="fa fa-gear fa-fw"></i>
						Gérer les code reviews <i class="fa fa-caret-down"></i>
				</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="/jee-training/add-promotion"><i
								class="fa fa-users fa-fw"></i> Ajouter une promotion</a></li>
						<li><a href="/jee-training/add-member"><i
								class="fa fa-user fa-fw"></i> Ajouter un membre</a></li>
						<li><a href="/jee-training/add-event"><i
								class="fa fa-calendar fa-fw"></i> Créer un rendez-vous</a></li>
					</ul></li>
			</ul>
		</nav>

		<div id="page-wrapper" class="container-fluid">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header">Modifier un membre</h1>
				</div>
				<!-- When a member tries to change his email address but another member already has it, 
                the controller sends a parameter status with a value of error. In this case an error message 
                	is displayed to the user-->
				<c:if test="${param.status eq 'error'}">
					<div class="col-lg-10 col-lg-offset-1">
						<div class="panel panel-danger">
							<div class="panel-heading">
								<h3 class="panel-title">Une erreur s'est produite ...</h3>
							</div>
							<div class="panel-body">L'adresse mail saisie est déjà
								prise, merci d'en saisir une autre.</div>
						</div>
					</div>
				</c:if>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<div class="row">
				<div class="col-lg-10 col-lg-offset-1">
					<!-- /.panel -->
					<div class="panel panel-default">
						<div class="panel-body">
							<div class="row">
								<div class="col-xs-12">
									<form action="update-member" method="post" class=""
										name="member-form">
										<div class="form-group">
											<label for="name">Nom</label> <input type="text"
												class="input-lg form-control" name="name" id="name"
												value="${member.name}">
										</div>
										<div class="form-group">
											<label for="email">Adresse Email</label> <input type="email"
												class="input-lg form-control" name="email" id="email"
												value="${member.email}">
										</div>
										<div class="form-group">
											<label for="bday">Date de naissance</label> <input
												name="bday" type="text" class="input-lg form-control"
												id="datetimepicker" required>
										</div>
										<div class="form-group">
											<label for="promotion">Promotion</label> <select
												name="promotion" class="input-lg form-control"
												id="promotion" required>
												<c:forEach var="promo" items="${promos}">
													<c:choose>
														<c:when
															test="${member.promo.promoName eq promo.promoName}">
															<option selected>${promo.promoName}</option>
														</c:when>
														<c:otherwise>
															<option>${promo.promoName}</option>
														</c:otherwise>
													</c:choose>
												</c:forEach>
											</select>
										</div>

										<div class="text-right">
											<button type="submit" class="btn btn-lg btn-primary">Modifier</button>
										</div>
									</form>
								</div>
							</div>
							<!-- /.row -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
		</div>
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->
	<footer class="footer">
		<div class="container">
			<div class="row text-center">
				<img src="img/ebusiness.png" class="logo" alt=""> &bullet;
				2017
			</div>
		</div>
	</footer>

	<!-- Custom JQuery -->
	<script src="js/jquery-1.12.4.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/form-validation/form-validation-event.js"></script>
	<script src="js/form-validation/form-validation-member.js"></script>
	<script src="js/form-validation/form-validation-promotion.js"></script>

	<!-- Bootstrap Core JavaScript -->
	<script src="bower_components/moment/min/moment-with-locales.min.js"></script>
	<script
		src="bower_components/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

	<!-- Custom Bootstrap DateTimePicker -->
	<script type="text/javascript">
       $(function () {
           $('#datetimepicker').datetimepicker({
               locale: 'fr',
               viewMode: 'years',
               format: 'DD/MM/YYYY',
               maxDate: '${now}'
           }).data('DateTimePicker').date('${member.birthdate}');
       });
    </script>

</body>

</html>
