// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "event-form"
  $("form[name='event-form']").validate({  
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      name: "required",
      dateTime: "required",
      description :"required",
      promotion : "required",
    },
    // Specify validation error messages
    messages: {
      name: "Veuillez saisir votre Nom",
      dateTime: "Veuillez choisir une date et une heure",
      description: {
        required: "Veuillez saisir une description à votre review",
        minlength: "Votre description doit être de 10 mots minimum"
      },
      promotion: "Veuillez choisir une promotion valide"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});