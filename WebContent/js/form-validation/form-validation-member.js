// Wait for the DOM to be ready
$(function() {
  // Initialize form validation on the registration form.
  // It has the name attribute "member-form"
  $("form[name='member-form']").validate({
    // Specify validation rules
    rules: {
      // The key name on the left side is the name attribute
      // of an input field. Validation rules are defined
      // on the right side
      name: "required",
      email: {required : true, 
    	  email : true
      },
      bday :"required",
      promotion : "required",
    },
    // Specify validation error messages
    messages: {
      name: "Veuillez saisir votre Nom",
      email: "Veuillez saisir une adresse mail valide",
      bday: {
        required: "Veuillez saisir une date de naissance",
      },
      promotion: "Veuillez choisir une promotion valide"
    },
    // Make sure the form is submitted to the destination defined
    // in the "action" attribute of the form when valid
    submitHandler: function(form) {
      form.submit();
    }
  });
});